- Construction type
	- on record?
- any wood anywhere, taints the type?
- basement exposed steel
  - protect this?
- Frontage increase
- Bar in basement?
	- a factor in allowable area determination?
- Fire Alarm
- sprinklers throughout the entire floor?
- 

---

- G515
![enter image description here](https://www.dropbox.com/s/kq7mrqnn2kn612p/2021-10-26_17-17-57_USG_-_Fire-Resistant_Assemblies_-_Bluebeam_Revu_x_Revu.png?dl=1)

- IBC
	- [506.2 Allowable Area Determination](https://up.codes/viewer/wisconsin/ibc-2015/chapter/5/general-building-heights-and-areas#506.2)
	- [707.3.10 Fire Areas](https://up.codes/viewer/wyoming/ibc-2015/chapter/7/fire-and-smoke-protection-features#707.3.10)
	- Section 903 Automatic Sprinkler Systems
		- [\[F\] 903.2.1 Group A](https://up.codes/viewer/wyoming/ibc-2015/chapter/9/fire-protection-systems#903.2.1)
			- [\[F\] 903.2.1.2 Group A-2](https://up.codes/viewer/wisconsin/ibc-2015/chapter/9/fire-protection-systems#903.2.1.2)
	- Section 907 Fire Alarm and Detection Systems
		- [\[F\] 907.2.1 Group A](https://up.codes/viewer/wyoming/ibc-2015/chapter/9/fire-protection-systems#907.2.1)
- Existing Building Code
	- [804.2 Automatic Sprinkler Systems](https://up.codes/viewer/wisconsin/iebc-2015/chapter/8/alterations-level-2#804.2)
		- [804.2.2 Groups A, B, E, F-1, H, I, M, R-1, R-2, R-4, S-1 and S-2](https://up.codes/viewer/wisconsin/iebc-2015/chapter/8/alterations-level-2#804.2.2)
	- [804.4 Fire Alarm and Detection](https://up.codes/viewer/wisconsin/iebc-2015/chapter/8/alterations-level-2#804.4)

- Definitions
	- [FIRE AREA](https://up.codes/viewer/wisconsin/ibc-2015/chapter/2/definitions#fire_area)


https://dps.mn.gov/divisions/sfm/programs-services/Documents/Sprinkler%20Applications/ConstructionTypeDefinitions.pdf
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY5OTY4MjkxOSwxNTUzODQ1NTMsLTEyNT
IwNjM4NiwtMjU0MTQ0NzkwLC04MDQxNTI4NzEsOTQxMzU2NTcy
LC0xODk3MDQ2OTQ5LC01ODgyNjMwNDIsLTE5Mzk4MTA5OTgsLT
E4NDc1ODQwNTksLTQ2NDAyNTM0OCwtMTk1NTIxNjc1NF19
-->